package sk.oxygene.stash.allpullrequests;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.project.Project;
import com.atlassian.stash.project.ProjectService;
import com.atlassian.stash.pull.*;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.user.UserService;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageImpl;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class AllPullRequestsServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(AllPullRequestsServlet.class);

    private final ProjectService projectService;
    private final PullRequestService pullRequestService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final StashAuthenticationContext stashAuthenticationContext;
    private final PageBuilderService pageBuilderService;

    private final WebResourceManager webResourceManager;

    public AllPullRequestsServlet(ProjectService projectService,
                                  PullRequestService pullRequestService,
                                  SoyTemplateRenderer soyTemplateRenderer,
                                  WebResourceManager webResourceManager,
                                  UserService userService, StashAuthenticationContext stashAuthenticationContext, PageBuilderService pageBuilderService) {
        this.projectService = projectService;
        this.pullRequestService = pullRequestService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webResourceManager = webResourceManager;
        this.stashAuthenticationContext = stashAuthenticationContext;
        this.pageBuilderService = pageBuilderService;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Project project;

        String[] path = request.getPathInfo().split("/");
        if (path.length == 2 && path[1].equals("all")) {
            project = null;
        } else if (path.length == 3 && path[1].equals("project") && !path[2].isEmpty()) {
            String projectKey = path[2];
            project = projectService.getByKey(projectKey);
        }
        else {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        PullRequestState state;
        boolean scopeToUser=false;
        String activeTab = request.getParameter("state");
        if (activeTab != null && activeTab.equals("merged")) {
            state = PullRequestState.MERGED;
        } else if (activeTab != null && activeTab.equals("declined")) {
            state = PullRequestState.DECLINED;
        } else if (activeTab != null && activeTab.equals("open")) {
            state = PullRequestState.OPEN;
        } else {
            state = PullRequestState.OPEN;
            scopeToUser=true;
            activeTab = "mine";
        }

        PageRequest pageRequest = new PageRequestImpl(0, 100);
        Page<PullRequest> pullRequestPage = findPullRequests(project, state, pageRequest,scopeToUser);

        Map<String, Object> context = Maps.newHashMap();
        context.put("pullRequestPage", pullRequestPage);
        context.put("activeTab", activeTab);
        context.put("currentUser", stashAuthenticationContext.getCurrentUser());

        String template;
        if (project == null) {
            webResourceManager.requireResourcesForContext("sk.oxygene.stash.stash-all-pull-requests.all");
            template = "plugin.page.allPullRequests";
        }
        else {
            webResourceManager.requireResourcesForContext("sk.oxygene.stash.stash-all-pull-requests.project");
            context.put("project", project);
            template = "plugin.page.projectPullRequests";
        }

        pageBuilderService.assembler().resources().requireWebResource("sk.oxygene.stash:tablesort.min.js");
        pageBuilderService.assembler().resources().requireWebResource("sk.oxygene.stash:all-pull-requests.css");

        response.setContentType("text/html; charset=UTF-8");
        try {
            soyTemplateRenderer.render(
                    response.getWriter(),
                    "sk.oxygene.stash.stash-all-pull-requests:server-side-soy",
                    template, context);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                throw new ServletException(e);
            }
        }
    }

    protected Page<PullRequest> findPullRequests(Project project, PullRequestState state, PageRequest pageRequest, boolean scopeToUser) {
        PullRequestSearchRequest searchRequest = (new PullRequestSearchRequest.Builder()).
                state(state).order(PullRequestOrder.NEWEST).build();

        if (project == null && !scopeToUser) {
            return pullRequestService.search(searchRequest, pageRequest);
        }

        // unfortunately, we can't use any PullRequestSearchRequest filter for this :/

        List<PullRequest> values = Lists.newLinkedList();
        boolean isLastPage = false;
        StashUser currentUser = stashAuthenticationContext.getCurrentUser();

        int offset = 0;
        PageRequest tmpPageRequest = new PageRequestImpl(0, 10);
        while (tmpPageRequest != null && !isLastPage) {
            Page<PullRequest> pullRequestPage = pullRequestService.search(searchRequest, tmpPageRequest);
            if (pullRequestPage.getIsLastPage()) {
                isLastPage = true;
            }
            for (PullRequest pullRequest : pullRequestPage.getValues()) {
                boolean includeForProject = project==null;
                boolean includeForUser = false;

                if (project!= null && pullRequest.getToRef().getRepository().getProject().getId().equals(project.getId())) {
                    includeForProject = true;
                }

                if(scopeToUser)
                {
                    if(pullRequest.getAuthor().getUser().getId().equals(currentUser.getId()))
                    {
                        includeForUser = true;
                    } else
                    {
                        for(PullRequestParticipant participant: pullRequest.getReviewers())
                        {
                            if(participant.getUser().getId().equals(currentUser.getId()))
                            {
                                includeForUser = true;
                            }
                        }
                    }
                }

                if ((scopeToUser && includeForUser && includeForProject) || (!scopeToUser && includeForProject)) {
                    values.add(pullRequest);
                }
                offset += 1;

            }
            tmpPageRequest = pullRequestPage.getNextPageRequest();
        }

        return new PageImpl<PullRequest>(pageRequest, values, isLastPage);
    }

}
