All Pull Requests Add-on for Atlassian Stash
============================================

*ABANDONED -- I no longer use Stash, so this plug-in isn't actively developed anymore. If you would like to take it over, please let me know.*

[![All Pull Requests](https://oxygene.sk/uploads/allpullrequests-small.png)](https://marketplace.atlassian.com/plugins/sk.oxygene.stash.stash-all-pull-requests)

Development
-----------

Useful Atlassian SDK commands:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK